import React from 'react';
import {DragSource, DropTarget} from 'react-dnd';
import _ from 'lodash';
import cn from 'classnames';
import { View } from 'react-native';

const NewTask = (props)=> {
    return _.flowRight(props.addDragSource, props.addDropTarget) (
        <View className={cn('Task', {
            'Task-drag': props.taskDragging,
            'Task-separator': props.taskSeparating,
        })}>
            <View className="Task-name">{props.name}</View>
        </View>
    );
}

export const MovableTask = _.flowRight([
    DropTarget(
        'Task',
        {
            hover(props, monitor) {
                const {taskId, taskIndex} = props;
                const draggingTask = monitor.getItem();
                if (draggingTask.id !== props.id) {
                    props.moveTask(draggingTask.id, taskId, taskIndex)
                }
            },
        },
        link => ({
            addDropTarget: link.dropTarget(),
        })
    ),
    DragSource(
        'Task',
        {
            beginDrag(props) {
                return {id: props.id};
            },

            isDragging(props, monitor) {
                return props.id === monitor.getItem().id;
            },
        },
        (link, monitor) => ({
            addDragSource: link.dragSource(),
            isDragging: monitor.isDragging(),
        })
    ),
])(NewTask);