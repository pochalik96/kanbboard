import React from 'react';
import { View } from 'react-native';
import EachPart from './EachPart';
import { MovableTask } from './NewTask';
import NewText from './NewText';

const DraggingBoard = ({tasks, parts, moveTask, addTask}) => {
    return(
        <View className="DraggingBoard">
            {parts.map(part => (
                <EachPart key={part.id} title={part.title} addTask={addTask.bind(null, part.id)}>
                    {part.taskIds
                        .map(taskId => tasks.find(task => task.id === taskId))
                        .map((task, index) => (
                            <MovableTask key={task.id} id={task.id} partId={part.id} partIndex={index} title={task.title} moveTask={moveTask}></MovableTask>
                        ))}
                    {part.taskIds.length === 0 && (
                        <MovableTask taskSeparating moveTask={taskId => moveTask(taskId, task.id, 0)}></MovableTask>
                    )}
                </EachPart>
            ))}
            <View className="Task">
                <NewText whenAccept={addTask} placeholder="Dodaj kolumnę"></NewText>
            </View>
        </View>
    )
}
export default DraggingBoard;