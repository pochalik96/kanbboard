import React from 'react';
import NewText from './NewText';
import { Text, View } from 'react-native';

const EachPart = (props) => {
    return (
        <View className="EachPart">
            <View className="Title"><Text>{props.title}</Text></View>
            {props.children}
            <NewText whenAccept={props.addTask} placeholder="Dodaj opis zadania" />  
        </View>
    );
}

export default EachPart;