import React, {Component} from 'react';
import { TextInput, View } from 'react-native';

class NewText extends Component {
    whenAccept = event => {
        const form = event.target;
        event.preventDefault();
        this.props.whenAccept(form.input.value);
        form.reset();
    };

    render() {
        return(
            <View whenAccept={this.whenAccept} ref={node => (this.form = node)}>
                <TextInput type="text" className="NewText-input" name="input" placeholder={this.props.placeholder}></TextInput>
            </View>
        );
    }
}

export default NewText;