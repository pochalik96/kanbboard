import React, { Component } from 'react';
import _ from 'lodash';
import DraggingBoard from './DraggingBoard';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd'

let _partId = 0;
let _taskId = 0;

const initTasks = Array.from({length: 9}).map(() => ({
  id: ++_taskId,
  title: `Task ${_taskId}`,
}));

const initParts = ['Open', 'In progress', 'Closed'].map((title, i) => ({
  id: _partId++,
  title,
  taskIds: initTasks.slice(i*3, i*3+3).map(task => task.id),
}));

const App = () => {
  state = {
    tasks: initTasks,
    parts: initParts,
  };

  addTask = (partId, _title) => {
    const title = _title.trim();
    if(!title) return;

    const newTask = {id: ++_taskId, title};
    this.setState(state => ({
      tasks: [...state.tasks, newTask],
      parts: state.parts.map(
        part => part.id === partId ? {...part, taskIds: [...part.taskIds, newTask.id]} : part
      ),
    }));
  };

  moveTask = (taskId, finalPartId, index) => {
    this.setState(state => ({
      parts: state.parts.map(part => ({
        ...part,
        taskIds: _.flowRight(
          ids => part.id === finalPartId ? [...ids.slice(0, index), taskId, ...ids.slice(index)] : ids,
          ids => ids.filter(id => id !== taskId)
        )(part.taskIds),
      })),
    }));
  };

  return (
    <DndProvider backend={HTML5Backend}>
          <DraggingBoard tasks={this.state.tasks} parts={this.state.parts} addTask={this.addTask} moveTask={this.moveTask}></DraggingBoard>
    </DndProvider>
  );
};

export default App;
